extends StaticBody2D

export var _speed = 5
export var _barkTimeLapse = 3

export var _upValue = 10
export var _downValue = 10
export var _leftValue = 10
export var _rightValue = 10

export var _startUp = true
export var _startRight = true
export var _damage = 50

var _startPositionY
var _startPositionX
var _goingDown = false
var _goingLeft = false

func _ready():
	$BarkTimer.start()
	_startPositionY = global_position.y
	_startPositionX = global_position.x

func _physics_process(_delta):
	if !_goingDown:
		goUp()
	else:
		goDown()
		
	if !_goingLeft:
		goLeft()
	else:
		goRight()

func goUp():
	if global_position.y > _startPositionY - _upValue:
		global_position.y -= _speed
	else:
		_goingDown = true
	
func goDown():
	if global_position.y < _startPositionY + _downValue:
		global_position.y += _speed
	else:
		_goingDown = false
		
func goLeft():
	if global_position.x > _startPositionX - _leftValue:
		global_position.x -= _speed
	else:
		_goingLeft = true
	
func goRight():
	if global_position.x < _startPositionX + _rightValue:
		global_position.x += _speed
	else:
		_goingLeft = false


func _on_Area2D_body_entered(body):
	if body.is_in_group("player"):
		body._hit(_damage)


func _on_BarkTimer_timeout():
	if($VisibilityNotifier2D.is_on_screen()):
		$SfxBark.play()

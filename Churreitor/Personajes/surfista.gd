extends StaticBody2D


export var changeTime = 5
var side = true

# Called when the node enters the scene tree for the first time.
func _ready():
	$Timer.start()

func _on_Timer_timeout():
	if side:
		$AnimatedSprite.animation = "front"
		$AnimationPlayer.play("tableFront")
		side = false
	else:
		$AnimatedSprite.animation = "side"
		$AnimationPlayer.play("tablaSide")
		side = true

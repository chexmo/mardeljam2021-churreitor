extends Area2D
class_name Client

const _states = {
	STATE_IDLE = 0,
	STATE_ASKING = 1,
	STATE_ANGRY = 2
}

export var _stateTimelapse = 1.5
export var _money : PackedScene = preload("res://Objetos/money.tscn")
var _actualState = _states.STATE_ASKING

func _ready():
	randomize()
	$StateTimer.wait_time = _stateTimelapse
	$StateTimer.start()
	set_animation()

func set_animation():
	match _actualState:
		_states.STATE_IDLE:
			$AnimatedSprite.animation = "idle"
		_states.STATE_ASKING:
			$AnimatedSprite.animation = "askingForChurro"
			if($VisibilityNotifier2D.is_on_screen()):
				$SfxGrito.play()
		_states.STATE_ANGRY:
			$AnimatedSprite.animation = "angry"

func _on_client_area_entered(area):
	if (area.is_in_group("throweable")&&_actualState == _states.STATE_ASKING):
		var churreli = area.catch()
		_actualState = _states.STATE_ANGRY
		set_animation()
		$SfxChurroHit.play()
		pay_churros()
		churreli.queue_free()

func pay_churros():
	call_deferred("drop_money")
	
func drop_money():
	var oMoneyA = _money.instance()
	var oMoneyB = _money.instance()
	var oMoneyC = _money.instance()
	get_tree().get_root().add_child(oMoneyA)
	get_tree().get_root().add_child(oMoneyB)
	get_tree().get_root().add_child(oMoneyC)
	oMoneyA.set_position($moneyPosA.global_position)
	oMoneyB.set_position($moneyPosB.global_position)
	oMoneyC.set_position($MoneyPosC.global_position)
	print("Churros paid")
	

func _on_StateTimer_timeout():
	_actualState = randi() %2
	set_animation()

extends Label

func set_objetivo(obj: int, inicial: int) -> void:
	text = "Don Churreitor.... Yo sé que hace mucho calor pero usted me prometió que iba a juntar $" \
		+ str(obj) \
		+ " para antes del mediodia, y solamente lleva $"\
		+ str(inicial)\
		+ " encima... \n¡Consiga lo que me prometió!"

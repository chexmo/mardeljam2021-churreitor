extends StaticBody2D

export var objetivo : int = 1000
export var inicial : int = 100

onready var hud = $"../../../HUD_Container/HUD"

signal charlar_con_churrero

func _ready() -> void:
	$Control/Label.set_objetivo(objetivo, inicial)
	hud.money = inicial

func _on_Area2D_body_entered(_body: Node) -> void:
	if hud.money >= objetivo:
		emit_signal("charlar_con_churrero")

extends Node2D

# Estos tuve que ponerlos asi para evitar la refe
export(String, FILE, "*.tscn") var next_scene = "res://Levels/LVL_End/LevelEnd.tscn"

func _ready() -> void:
	Global.game_state = "JUGANDO"

func _on_HUD_game_ended() -> void:
	Global.game_state = "PERDISTE"
	get_tree().change_scene(next_scene)

func _on_Jefe_charlar_con_churrero() -> void:
	Global.game_state = "GANASTE"
	get_tree().change_scene(next_scene)

func _on_Churrero_money(a) -> void:
	$HUD_Container/HUD.increment_money(a)

extends Control

export var next_scene : PackedScene

func _ready() -> void:
	$Label.text = Global.game_state


func _on_Timer_timeout() -> void:
	get_tree().change_scene_to(next_scene)
